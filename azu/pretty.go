package azu

import (
	"encoding/json"
	"fmt"
	"github.com/tidwall/pretty"
)

func PrettyColor(json []byte) []byte {
	return pretty.Color(pretty.Pretty(json), nil)
}

func PrettyColorString(json []byte) string {
	return string(pretty.Color(pretty.Pretty(json), nil))
}

func PrettyJSON(v interface{}) string {
	jsonb, _ := json.Marshal(v)
	json := pretty.Color(pretty.Pretty(jsonb), nil)
	return string(json)
}

func PrettyPrint(v interface{}) {
	fmt.Println()
	fmt.Println(PrettyJSON(v))
	fmt.Println()
}
