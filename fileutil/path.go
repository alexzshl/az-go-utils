package fileutil

import (
	"bufio"
	"errors"
	"io"
	"os"
)

// PathExists 判断文件或目录是否存在
// 根据路径获取文件描述符:
// 如果未返回错误, 表示存在
// 如果返回错误且错误信息指示了不存在, 则返回 false,nil;如果是其他错误类型返回false,err
func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func CopyFile(srcFilePath string, destFilePath string) (int64, error) {
	srcfile, err := os.Open(srcFilePath)
	if err != nil {
		// fmt.Println("open src file error:", err)
		return -1, errors.New("open src file error")
	}
	defer srcfile.Close()
	reader := bufio.NewReader(srcfile)

	destfile, err := os.OpenFile(destFilePath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		// fmt.Println("open dest file error:", err)
		return -1, errors.New("open dest file error")
	}
	defer destfile.Close()
	writer := bufio.NewWriter(destfile)

	return io.Copy(writer, reader)
}
