package executil

import (
	"fmt"
	"os/exec"
	"runtime"
)

var commands = map[string]string{
	"windows": "explorer",
	"darwin":  "open",
	"linux":   "xdg-open",
}

// OpenURI Open calls the OS default program for uri
func OpenURI(uri string) error {
	command, ok := commands[runtime.GOOS]
	if !ok {
		return fmt.Errorf("don't know how to open things on %s platform", runtime.GOOS)
	}

	cmd := exec.Command(command, uri)

	defer fmt.Printf("open %s ", uri)
	return cmd.Start()
}
